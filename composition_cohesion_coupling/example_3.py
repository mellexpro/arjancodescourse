"""
Example 3
"""

from dataclasses import dataclass, field
from typing import List, Protocol


class PaymentSouce(Protocol):
    """Interface of the payment source type for various contracts."""

    def calculate_payment(self) -> int:
        """Calculate payment of the contract type"""
        ...


@dataclass
class Employee:
    """Represents an employee and it's information"""

    name: str = ""
    id_no: int = 0
    payment_sources: List[PaymentSouce] = field(default_factory=list)

    def add_payment_source(self, payment_source: PaymentSouce):
        """Add a payment source to the employee."""
        self.payment_sources.append(payment_source)

    def compute_pay(self) -> int:
        """Return the total wage rearned by the employee."""
        return int(sum(x.calculate_payment() for x in self.payment_sources))


@dataclass
class ComissionBasedContract():
    """Represent a payment source type that pays a comission based on
    comission rate and contracts landed.
    """

    rate: int = 10000
    deals_landed: int = 0

    def calculate_payment(self) -> int:
        """Calculate the total comission earned."""
        return int(self.rate * self.deals_landed)


@dataclass
class HourlyContract():
    """Represents a payment source type that pays per hour based on
    the pay rate, hours worked and employer cost.
    """

    pay_rate: int = 0
    hours_worked: float = 0.0
    employer_cost: int = 100_000

    def calculate_payment(self) -> int:
        """Calculate the hourly wage earned."""
        return int((self.pay_rate * self.hours_worked) + self.employer_cost)


@dataclass
class SalaryContract():
    """Represents a payment source type that pays a salary based on
    monthly salary and an incremental percentage.
    """

    monthly_salary: int = 0
    percentage: float = 1.0

    def calculate_payment(self) -> int:
        """Calculate the salary wage earned."""
        return int(self.monthly_salary * self.percentage)


@dataclass
class FreelancerContract():
    """Represents a payment source type that pays a freelancer wage based on
    pay rate, hours worked. The freelancer could be vat registered.
    """

    pay_rate: float = 0.0
    hours_worked: int = 0
    vat_number: str = ""

    def calculate_payment(self) -> int:
        """Calculate the frelancer wage earned."""
        return int(self.pay_rate * self.hours_worked)


def create_employees():
    """Helper function to run the example."""

    henry = Employee(name="Henry", id_no=12346)
    henry.add_payment_source(HourlyContract(pay_rate=5000, hours_worked=100))
    print(f"{henry.name} earned ${(henry.compute_pay() / 100):.2f}")

    sarah = Employee(name="Sarah", id_no=47832)
    sarah.add_payment_source(SalaryContract(monthly_salary=500_000))
    sarah.add_payment_source(ComissionBasedContract(deals_landed=10))
    print(f"{sarah.name} earned ${(sarah.compute_pay() / 100):.2f}")

    jack = Employee(name="Jack", id_no=10102)
    jack.add_payment_source(FreelancerContract(pay_rate=8000, hours_worked=120, vat_number="12345678790"))
    jack.add_payment_source(ComissionBasedContract(deals_landed=5))
    print(f"{jack.name} earned ${(jack.compute_pay() / 100):.2f}")
