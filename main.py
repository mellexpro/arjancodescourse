"""
Main code executor, used to trigger different code sections.
"""

from types_and_datastructures.example_1 import create_vehicles as cv1
from types_and_datastructures.example_2 import create_vehicles as cv2
from composition_cohesion_coupling.example_3 import create_employees as ce1


callables = {
    "TypesAndDatastructuresExample1": cv1,
    "TypesAndDatastructuresExample2": cv2,
    "CompositionCohesionCouplingExample3": ce1,
}


if __name__ == "__main__":

    # Types and datastructures. Example 1.
    # callables["TypesAndDatastructuresExample1"]()

    # Types and datastructures. Example 1.
    # callables["TypesAndDatastructuresExample2"]()

    # Composition Cohesion Coupling. Example 3.
    callables["CompositionCohesionCouplingExample3"]()
