"""
Example 1.
"""
from datetime import datetime
from enum import Enum, auto
from typing import List
import random
import string
from dataclasses import dataclass, field



def generate_vehicle_license() -> str:
    """Helper function to generate vehicle license."""
    digit_part = "".join(random.choices(string.digits, k=2))
    letter_part_1 = "".join(random.choices(string.ascii_uppercase, k=2))
    letter_part_2 = "".join(random.choices(string.ascii_uppercase, k=2))
    return f"{digit_part}-{letter_part_1}-{letter_part_2}"


class AccessoryType(Enum):
    """Different types of accessories for a car."""
    AIRCON = auto()
    CRUISECONTROL = auto()
    NAVIGATION = auto()
    OPENROOF = auto()
    BATHTUB = auto()
    MINIBAR = auto()


class FuelType(Enum):
    """Types of fuel for a car."""
    PETROL = auto()
    DIESEL = auto()
    ELECTRIC = auto()


@dataclass
class Vehicle:
    """A vehicle and it's details."""

    brand: str
    model: str
    colour: str
    license_plate: str = field(init=False)
    accesories: List[AccessoryType] = field(default_factory=list)
    fuel_type: FuelType = FuelType.ELECTRIC

    def __post_init__(self):
        """Perform some action after init."""
        self.license_plate = generate_vehicle_license()
        if self.brand == "Tesla":
            self.license_plate += "-t"


    def reserve(self, date: datetime):
        """Resevere a vehicle for a later date."""
        print(f"Vehicle is reserved for {date}.")



def create_vehicles():
    """Create a couple of vehicles."""

    tesla = Vehicle(
        brand="Tesla",
        model="Model 3",
        colour="black",
        accesories=[
            AccessoryType.AIRCON,
            AccessoryType.MINIBAR,
            AccessoryType.NAVIGATION,
            AccessoryType.CRUISECONTROL
        ]
    )

    volkswagen = Vehicle(
        brand="Volkswagen",
        model="ID3",
        colour="white"
    )

    bmw = Vehicle(
        brand="BMW",
        model="520c",
        colour="blue",
        fuel_type=FuelType.PETROL,
        accesories=[
            AccessoryType.NAVIGATION,
            AccessoryType.CRUISECONTROL
        ]
    )

    print(tesla)
    print(volkswagen)
    print(bmw)

    bmw.reserve(date=datetime.now())
