"""
Example 2.
"""

from abc import ABC, abstractmethod
import math
from dataclasses import dataclass
from datetime import datetime
from typing import Protocol


class Rentable(Protocol):
    """Define a retable vehicle contract."""

    def reserve(self, start_date: datetime, days: int):
        """Reserve vehicle protocol method."""
        ...


class LicenseHolder(Protocol):
    """Define a lincese holder contract."""

    def register(self, date: datetime):
        """register a license protocol method."""
        ...

    def renew(self):
        """renew a license protocol method."""
        ...


@dataclass
class VehicleRegistration:
    """Base behaviour for how to handle vehicle registration."""

    model: str = ""

    def __post_init__(self):
        """After initialization."""
        self.register(datetime.now())

    def register(self, date: datetime):
        """Register a new vehicle license"""
        print(f"Registering {self.model} a new license on this date: {date}")

    def renew(self):
        """Renew a license to the current date."""
        print(f"Renewing {self.model} license on this date: {datetime.now()}")


@dataclass
class VehicleReservation(ABC):
    """Base behaviour for how a vehicle is reserved."""

    is_reserved: bool = False

    @abstractmethod
    def reserve(self, start_date: datetime, days: int):
        """Reserve a vehicle for a number of days."""
        ...


@dataclass
class Car(VehicleRegistration, VehicleReservation):
    """Car vehicle implementation."""

    def reserve(self, start_date: datetime, days: int):
        """Reserve a Car for a number of days."""
        self.is_reserved = True
        print(f"Reserving car {self.model} for {days} days at date {start_date}.")


@dataclass
class Truck(VehicleRegistration, VehicleReservation):
    """Truck vehicle implementation."""

    has_trailer: bool = False

    def reserve(self, start_date: datetime, days: int):
        """Reserve a Truck for a number of days."""
        months = math.ceil(days / 30)
        self.is_reserved = True
        self.has_trailer = True
        print(f"Reserving truck {self.model} for {months} months at date {start_date}, including a trailer.")


def reserve_now(vehicle: Rentable, days: int):
    """Reserve the specified vehicle for the supplied days."""
    vehicle.reserve(datetime.now(), days)


def renew_now(vehicle: LicenseHolder):
    """Renew the license of the supplied vehicle"""
    vehicle.renew()


def create_vehicles():
    """Create test vehicles."""

    car = Car(model="Tesla")
    truck = Truck(model="Volvo")

    print(car)
    print(truck)

    reserve_now(car, 15)
    reserve_now(truck, 52)

    print(car)
    print(truck)

    renew_now(car)
    renew_now(truck)
